<?php 

if(App::offsetExists('api.router')) {
	$api = app('api.router');

	$api->version('v1', function ($api) {
		$api->get('/app', 'Kodeartisan\Appkode4\Api\Http\Controllers\AppsController@index');
		$api->get('/app/{appKey}/detail', 'Kodeartisan\Appkode4\Api\Http\Controllers\AppsController@detail');
		$api->post('/agent/create',  'Kodeartisan\Appkode4\Api\Http\Controllers\AgentController@store');
	});
} 


