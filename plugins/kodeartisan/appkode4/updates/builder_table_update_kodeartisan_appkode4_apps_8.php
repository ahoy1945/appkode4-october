<?php namespace Kodeartisan\Appkode4\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKodeartisanAppkode4Apps8 extends Migration
{
    public function up()
    {
        Schema::table('kodeartisan_appkode4_apps', function($table)
        {
            $table->string('icon')->nullable();
            $table->string('app_key');
            $table->string('name');
            $table->string('package');
            $table->integer('category_id')->unsigned();
            $table->string('status');
            $table->boolean('is_promoted');
            $table->string('images')->nullable();
            $table->boolean('show_ads')->nullable();
            $table->dateTime('show_ads_date');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('kodeartisan_appkode4_apps', function($table)
        {
            $table->dropColumn('icon');
            $table->dropColumn('app_key');
            $table->dropColumn('name');
            $table->dropColumn('package');
            $table->dropColumn('category_id');
            $table->dropColumn('status');
            $table->dropColumn('is_promoted');
            $table->dropColumn('images');
            $table->dropColumn('show_ads');
            $table->dropColumn('show_ads_date');
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });
    }
}
