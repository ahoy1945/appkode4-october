<?php namespace Kodeartisan\Appkode4\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKodeartisanAppkode4Apps4 extends Migration
{
    public function up()
    {
        Schema::table('kodeartisan_appkode4_apps', function($table)
        {
            $table->string('app_key');
            $table->increments('id')->nullable(false)->unsigned(false)->default(null)->change();
            $table->renameColumn('category_id_id', 'category_id');
        });
    }
    
    public function down()
    {
        Schema::table('kodeartisan_appkode4_apps', function($table)
        {
            $table->dropColumn('app_key');
            $table->string('id', 255)->nullable(false)->unsigned(false)->default(null)->change();
            $table->renameColumn('category_id', 'category_id_id');
        });
    }
}
