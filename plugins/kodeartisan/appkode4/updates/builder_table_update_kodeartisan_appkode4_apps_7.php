<?php namespace Kodeartisan\Appkode4\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKodeartisanAppkode4Apps7 extends Migration
{
    public function up()
    {
        Schema::table('kodeartisan_appkode4_apps', function($table)
        {
            $table->dropColumn('category_id');
            $table->dropColumn('name');
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->dropColumn('package');
            $table->dropColumn('is_promoted');
            $table->dropColumn('status');
            $table->dropColumn('deleted_at');
            $table->dropColumn('app_key');
            $table->dropColumn('images');
            $table->dropColumn('icon');
            $table->dropColumn('show_ads');
            $table->dropColumn('show_ads_date');
        });
    }
    
    public function down()
    {
        Schema::table('kodeartisan_appkode4_apps', function($table)
        {
            $table->integer('category_id')->unsigned();
            $table->string('name', 255);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('package', 255);
            $table->boolean('is_promoted')->default(0);
            $table->string('status', 255)->default('draft');
            $table->timestamp('deleted_at')->nullable();
            $table->string('app_key', 255);
            $table->string('images', 255)->nullable();
            $table->string('icon', 255)->nullable();
            $table->boolean('show_ads');
            $table->dateTime('show_ads_date');
        });
    }
}
