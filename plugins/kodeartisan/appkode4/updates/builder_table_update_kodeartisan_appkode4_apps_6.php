<?php namespace Kodeartisan\Appkode4\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKodeartisanAppkode4Apps6 extends Migration
{
    public function up()
    {
        Schema::table('kodeartisan_appkode4_apps', function($table)
        {
            $table->string('icon')->nullable();
            $table->boolean('show_ads');
            $table->dateTime('show_ads_date');
        });
    }
    
    public function down()
    {
        Schema::table('kodeartisan_appkode4_apps', function($table)
        {
            $table->dropColumn('icon');
            $table->dropColumn('show_ads');
            $table->dropColumn('show_ads_date');
        });
    }
}
