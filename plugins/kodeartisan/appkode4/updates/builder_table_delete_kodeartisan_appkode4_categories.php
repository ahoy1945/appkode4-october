<?php namespace Kodeartisan\Appkode4\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteKodeartisanAppkode4Categories extends Migration
{
    public function up()
    {
        Schema::dropIfExists('kodeartisan_appkode4_categories');
    }
    
    public function down()
    {
        Schema::create('kodeartisan_appkode4_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 255);
            $table->timestamp('created_at')->default('CURRENT_TIMESTAMP');
            $table->timestamp('updated_at')->default('0000-00-00 00:00:00');
        });
    }
}
