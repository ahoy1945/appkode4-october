<?php namespace Kodeartisan\Appkode4\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKodeartisanAppkode4Categories extends Migration
{
    public function up()
    {
        Schema::table('kodeartisan_appkode4_categories', function($table)
        {
            $table->increments('id')->change();
        });
    }
    
    public function down()
    {
        Schema::table('kodeartisan_appkode4_categories', function($table)
        {
            $table->integer('id')->change();
        });
    }
}
