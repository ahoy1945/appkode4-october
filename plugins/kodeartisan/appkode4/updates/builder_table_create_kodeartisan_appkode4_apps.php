<?php namespace Kodeartisan\Appkode4\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateKodeartisanAppkode4Apps extends Migration
{
    public function up()
    {
        Schema::create('kodeartisan_appkode4_apps', function($table)
        {
            $table->engine = 'InnoDB';
            $table->string('id');
            $table->integer('category_id')->unsigned();
            $table->string('name');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->primary(['id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('kodeartisan_appkode4_apps');
    }
}
