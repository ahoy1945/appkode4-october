<?php namespace Kodeartisan\Appkode4\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKodeartisanAppkode4Apps extends Migration
{
    public function up()
    {
        Schema::table('kodeartisan_appkode4_apps', function($table)
        {
            $table->dropColumn('deleted_at');
        });
    }
    
    public function down()
    {
        Schema::table('kodeartisan_appkode4_apps', function($table)
        {
            $table->timestamp('deleted_at')->nullable();
        });
    }
}
