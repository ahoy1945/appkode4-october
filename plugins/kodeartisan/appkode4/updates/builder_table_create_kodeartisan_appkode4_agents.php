<?php namespace Kodeartisan\Appkode4\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateKodeartisanAppkode4Agents extends Migration
{
    public function up()
    {
        Schema::create('kodeartisan_appkode4_agents', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('app_id')->unsigned();
            $table->string('ip_address')->nullable();
            $table->string('os_name')->nullable();
            $table->string('os_api_version')->nullable();
            $table->string('phone_manufacturer')->nullable();
            $table->string('phone_model')->nullable();
            $table->string('phone_screen_size')->nullable();
            $table->string('network_operator_name')->nullable();
            $table->string('network_country_iso')->nullable();
            $table->string('network_type')->nullable();
            $table->boolean('is_rooted')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('kodeartisan_appkode4_agents');
    }
}
