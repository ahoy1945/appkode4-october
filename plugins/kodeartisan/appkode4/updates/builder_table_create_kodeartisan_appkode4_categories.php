<?php namespace Kodeartisan\Appkode4\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateKodeartisanAppkode4Categories extends Migration
{
    public function up()
    {
        Schema::create('kodeartisan_appkode4_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('kodeartisan_appkode4_categories');
    }
}
