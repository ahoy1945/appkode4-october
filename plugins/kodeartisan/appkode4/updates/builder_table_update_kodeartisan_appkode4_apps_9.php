<?php namespace Kodeartisan\Appkode4\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKodeartisanAppkode4Apps9 extends Migration
{
    public function up()
    {
        Schema::table('kodeartisan_appkode4_apps', function($table)
        {
            $table->string('id', 10)->nullable(false)->unsigned(false)->default(null)->change();
            $table->dropColumn('app_key');
        });
    }
    
    public function down()
    {
        Schema::table('kodeartisan_appkode4_apps', function($table)
        {
            $table->increments('id')->nullable(false)->unsigned(false)->default(null)->change();
            $table->string('app_key', 255);
        });
    }
}
