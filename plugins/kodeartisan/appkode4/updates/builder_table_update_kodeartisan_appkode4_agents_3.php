<?php namespace Kodeartisan\Appkode4\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKodeartisanAppkode4Agents3 extends Migration
{
    public function up()
    {
        Schema::table('kodeartisan_appkode4_agents', function($table)
        {
            $table->integer('app_id')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('kodeartisan_appkode4_agents', function($table)
        {
            $table->string('app_id', 255)->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
}
