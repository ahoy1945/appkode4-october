<?php namespace Kodeartisan\Appkode4\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKodeartisanAppkode4Agents extends Migration
{
    public function up()
    {
        Schema::table('kodeartisan_appkode4_agents', function($table)
        {
            $table->string('app_key');
            $table->dropColumn('app_id');
        });
    }
    
    public function down()
    {
        Schema::table('kodeartisan_appkode4_agents', function($table)
        {
            $table->dropColumn('app_key');
            $table->integer('app_id')->unsigned();
        });
    }
}
