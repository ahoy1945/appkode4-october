<?php namespace Kodeartisan\Appkode4\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKodeartisanAppkode4Agents2 extends Migration
{
    public function up()
    {
        Schema::table('kodeartisan_appkode4_agents', function($table)
        {
            $table->renameColumn('app_key', 'app_id');
        });
    }
    
    public function down()
    {
        Schema::table('kodeartisan_appkode4_agents', function($table)
        {
            $table->renameColumn('app_id', 'app_key');
        });
    }
}
