<?php namespace Kodeartisan\Appkode4\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKodeartisanAppkode4Apps3 extends Migration
{
    public function up()
    {
        Schema::table('kodeartisan_appkode4_apps', function($table)
        {
            $table->renameColumn('category_id', 'category_id_id');
        });
    }
    
    public function down()
    {
        Schema::table('kodeartisan_appkode4_apps', function($table)
        {
            $table->renameColumn('category_id_id', 'category_id');
        });
    }
}
