<?php namespace Kodeartisan\Appkode4\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKodeartisanAppkode4Apps2 extends Migration
{
    public function up()
    {
        Schema::table('kodeartisan_appkode4_apps', function($table)
        {
            $table->string('package');
            $table->boolean('is_promoted')->default(0);
            $table->string('status')->default('draft');
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('kodeartisan_appkode4_apps', function($table)
        {
            $table->dropColumn('package');
            $table->dropColumn('is_promoted');
            $table->dropColumn('status');
            $table->dropColumn('deleted_at');
        });
    }
}
