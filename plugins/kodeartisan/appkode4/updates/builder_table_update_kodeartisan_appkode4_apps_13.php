<?php namespace Kodeartisan\Appkode4\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKodeartisanAppkode4Apps13 extends Migration
{
    public function up()
    {
        Schema::table('kodeartisan_appkode4_apps', function($table)
        {
            $table->boolean('show_ads_is_active')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('kodeartisan_appkode4_apps', function($table)
        {
            $table->dropColumn('show_ads_is_active');
        });
    }
}
