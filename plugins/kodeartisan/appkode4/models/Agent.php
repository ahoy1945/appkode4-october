<?php namespace Kodeartisan\Appkode4\Models;

use Model;

/**
 * Model
 */
class Agent extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'kodeartisan_appkode4_agents';

    protected $fillable = ['app_id', 'ip_address', 'os_name', 'os_api_version', 'phone_manufacturer', 'phone_model', 'phone_screen_size', 'network_operator_name', 'network_country_iso', 'network_type', 'is_rooted','created_at', 'updated_at', ];

    public $belongsTo = [
        'app' => ['Kodeartisan\Appkode4\Models\App', 'id', 'app_id']
    ];
}