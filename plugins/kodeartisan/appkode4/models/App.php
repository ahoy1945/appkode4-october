<?php namespace Kodeartisan\Appkode4\Models;

use Model;
use Ramsey\Uuid\Uuid;

/**
 * Model
 */
class App extends Model
{
    use \October\Rain\Database\Traits\Validation;
    /*
     * Validation
     */
    public $rules = [
      /*  'name' => 'required',
        'package' => 'required'*/
    ];

    public $jsonable = ['images'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'kodeartisan_appkode4_apps';

    public $belongsTo = [
        'category' => ['Kodeartisan\Appkode4\Models\Category']
    ];

    public function beforeCreate()
    {
        $this->app_key = Uuid::uuid4()->toString();
    }

   
}