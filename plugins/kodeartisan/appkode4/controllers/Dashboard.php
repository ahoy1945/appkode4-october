<?php namespace Kodeartisan\Appkode4\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Backend\Widgets\ReportContainer;

class Dashboard extends Controller
{
    
    
    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Kodeartisan.Appkode4', 'appkode4', 'dashboard');

        $this->addCss('/modules/backend/assets/css/dashboard/dashboard.css', 'core');
    }

    public function index()
    {
    	$this->initReportContainer();

    	$this->pageTitle = 'Dashboard';
    }



    protected function initReportContainer()
    {
        new ReportContainer($this, 'config_dashboard.yaml');
    }

    public function index_onInitReportContainer()
    {
        $this->initReportContainer();

        return ['#dashReportContainer' => $this->widget->reportContainer->render()];
    }
}