<?php namespace Kodeartisan\Appkode4;

use System\Classes\PluginBase;
use Illuminate\Contracts\Http\Kernel;
use Illuminate\Foundation\AliasLoader;
use Backend;
use Log;
use Kodeartisan\Appkode4\Models\App as AppModel;

class Plugin extends PluginBase
{

	/**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
	public function register()
	{
		$this->app['config']['api'] = require __DIR__ . '/config/api.php';

		$this->app->register(\Dingo\Api\Provider\LaravelServiceProvider::class);

		$alias = AliasLoader::getInstance();

		

	}

    public function registerSchedule($schedule)
    {
        $schedule->call(function () {
            AppModel::where('show_ads_date', new\DateTime('today'))->where('show_ads', true)->where('show_ads_is_active', false)->update(['show_ads_is_active' => true]);
        })->hourly();
    }

    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }

    public function boot()
    {
        $this->app->bind('Kodeartisan\Appkode4\Api\Repositories\Agent\AgentInterface','Kodeartisan\Appkode4\Api\Repositories\Agent\EloquentAgentRepository');
        $this->app->bind('Kodeartisan\Appkode4\Api\Repositories\App\AppInterface','Kodeartisan\Appkode4\Api\Repositories\App\EloquentAppRepository');
    	require realpath(__DIR__ . '/routes.php');
    }

    public function registerNavigation()
    {
        return [
            'appkode4' => [
                'label'       => 'Appkode4',
                'url'         => Backend::url('kodeartisan/appkode4/dashboard'),
                'icon'        => 'icon-android',
                'order'       => 500,

                'sideMenu' => [
                    'dashboard' => [
                        'label'       => 'Dashboard',
                        'url'         => Backend::url('kodeartisan/appkode4/dashboard'),
                        'icon'        => 'icon-bars'
                    ],
                    'apps' => [
                        'label'       => 'Apps',
                        'url'         => Backend::url('kodeartisan/appkode4/apps'),
                        'icon'        => 'icon-android'
                    ],
                    'categories' => [
                        'label'       => 'Categories',
                        'url'         => Backend::url('kodeartisan/appkode4/category'),
                        'icon'        => 'icon-tags'
                    ],
                    'agents' => [
                        'label'       => 'Agents',
                        'url'         => Backend::url('kodeartisan/appkode4/agents'),
                        'icon'        => 'icon-users'
                    ]
                ]
            ]
        ];
    }
}
