<?php namespace Kodeartisan\Appkode4\Api\Transformers;

use Kodeartisan\Appkode4\Models\Agent;
use League\Fractal\TransformerAbstract;

class AgentsTransformer extends TransformerAbstract
{
	public function transform(Agent $agent)
	{
		return [
			'id' => (int)$agent->id,
			'os_name' => $agent->os_name,
			'ip_address' => $agent->ip_address,
			'os_api_version' => $agent->os_api_version,
			'phone_manufacturer' => $agent->phone_manufacturer,
			'phone_model' => $agent->phone_model,
			'phone_screen_size' => $agent->phone_screen_size,
			'network_operator_name' => $agent->network_operator_name,
			'network_country_iso' => $agent->network_country_iso,
			'network_type' => $agent->network_type,
			'is_rooted' => (bool)$agent->is_rooted
		]; 
	}
}