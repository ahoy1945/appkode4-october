<?php namespace Kodeartisan\Appkode4\Api\Transformers;

use Kodeartisan\Appkode4\Models\App;
use League\Fractal\TransformerAbstract;

class AppsTransformer extends TransformerAbstract
{
	public function transform(App $app)
	{
		return [
			'id' => (int)$app->id,
			'app_key' => $app->app_key,
			'package' => $app->package,
			'is_promoted' => (bool) $app->is_promoted,
			'status' => $app->status,
			'icon' => url('/storage/app/media'.$app->icon),
			'images' => $app->images
		]; 
	}
}