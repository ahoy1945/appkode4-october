<?php namespace Kodeartisan\Appkode4\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Kodeartisan\Appkode4\Api\Repositories\App\AppInterface;
use Kodeartisan\Appkode4\Api\Transformers\AppsTransformer;
use Dingo\Api\Routing\Helpers;

class AppsController extends Controller
{
	use Helpers;

	protected $appsRepository;

	public function __construct(AppInterface $appsRepository)
	{
		$this->appsRepository = $appsRepository;
	}

	public function index(Request $request)
    {
    	return $this->appsRepository->all($request);
    }

	public function detail($appKey)
	{
		$app = $this->appsRepository->getItemBy('app_key', $appKey);

        $response = ($app ? $this->response->item($app, new AppsTransformer) : $this->response->errorNotFound());

        return $response;
	}
}