<?php namespace Kodeartisan\Appkode4\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Kodeartisan\Appkode4\Api\Repositories\Agent\AgentInterface;
use Kodeartisan\Appkode4\Api\Transformers\AgentsTransformer;
use Dingo\Api\Routing\Helpers;

class AgentController extends Controller
{
	use Helpers;

	protected $agentRepository;

	public function __construct(AgentInterface $agentRepository)
	{
		$this->agentRepository = $agentRepository;
	}

	public function detail(Request $request)
	{

		$storeData = $this->agentRepository->create($request->all());

		return ($storeData ? $this->response->item($storeData, new AgentsTransformer) : $this->response->errorInternal());
		
	}

	public function store(Request $request)
    {
        $storeData = $this->agentRepository->create($request->all());

        return ($storeData ? $this->response->item($storeData, new AgentsTransformer) : $this->response->errorInternal());
      
    }
}