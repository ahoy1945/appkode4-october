<?php namespace Kodeartisan\Appkode4\Api\Repositories\Agent;

use Kodeartisan\Appkode4\Models\Agent;
use Kodeartisan\Appkode4\Models\App;

class EloquentAgentRepository implements AgentInterface
{
	protected $agentModel;

	protected $appModel;

	public function __construct(Agent $agent, App $app)
	{
		$this->agentModel = $agent;
		$this->appModel = $app;
	}

	public function create($data = [])
	{
		
		$app = $this->appModel->whereAppKey($data['app_key'])->first();

		if($app) {
			$data['app_id'] = $app->id;
			$query = $this->agentModel->create($data);
			return $query;
		}

		 
		
	}
	 
}