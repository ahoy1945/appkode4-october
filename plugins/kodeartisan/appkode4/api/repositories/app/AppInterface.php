<?php namespace Kodeartisan\Appkode4\Api\Repositories\App;

interface AppInterface
{
	public function all($request);
	public function getItemBy($attribute, $value, $columns = ['*']);
}