<?php namespace Kodeartisan\Appkode4\Api\Repositories\App;

use Kodeartisan\Appkode4\Models\App;
use Kodeartisan\Appkode4\Api\Transformers\AppsTransformer;
use Dingo\Api\Routing\Helpers;

class EloquentAppRepository implements AppInterface
{
	use Helpers ;

	/**
	 * @var App\App;
	 */
	protected $appModel;
	/**
	 * @var integer
	 */
	protected $isLimit = false;
	/**
	 * @var integer
	 */
	protected $limit = 10;
	/**
	 * @var string
	 */
	protected $status = '';
	/**
	 * @var string
	 */
	protected $category = "";
	/**
	 * @var boolean
	 */
	protected $isPromoted = false;
	/**
	 * @var string
	 */
	protected $orderBy = 'DESC';
	/**
	 * @var boolean
	 */
	protected $random = false;
	/**
	 * @var array
	 */
	protected $columns = ['*'];
	/**
	 * @var string
	 */
	protected $keyword = "";
	 
	
	public function __construct(App $appModel)
	{
		$this->appModel = $appModel;
	}

	public function all($request)
	{	
		$this->initRequest($request);

		$query = $this->appModel->with('category');
 
	 	if(!empty($this->category))
	 		$query->whereHas('category', function($queryChild) {
	 			$queryChild->where('name', $this->category);
	 		});

	 	if($this->random)
			$query = $query->orderByRaw("RAND()");

		if($this->isLimit)
			$query = $query->limit($this->limit);

		if($this->isPromoted)
			$query = $query->where('is_promoted', $this->isPromoted);

	 	$data =  $query->get($this->columns);

	 	return $this->response->collection($data, new AppsTransformer);
	}

	private function initRequest($request)
	{

		$this->isPromoted = ($request->has('promoted') && $request->input('promoted') == 'true' ? true : $this->isPromoted);

		$this->isLimit = ($request->has('limit') ? true :  $this->isLimit);
		
		$this->limit = ($request->has('limit') ? $request->input('limit') : $this->isLimit);
		
		$this->random = ($request->has('random') && $request->input('random') == 'true'  ? true : $this->random);
		
		$this->orderBy = ($request->has('order_by') ? $request->input('order_by') : $this->orderBy);
		
		$this->category = ($request->has('category') ? $request->input('category') : $this->category);
		
		$this->status = ($request->has('status') ? $request->input('status') : $this->status);

		
	}

	 
	public function getItemBy($attribute, $value, $columns = ['*'])
	{
		$query = $this->appModel->where($attribute,$value)->first($columns);

		return $query;
	}
	
}